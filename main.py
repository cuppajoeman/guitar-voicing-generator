import math
from pprint import pprint

from instmuse.stringed_instrument import GuitarVoicing

GUITAR_STRINGS = [0, 1, 2, 3, 4, 5]

MAJOR_DIATONIC = [
    (0, 4, 7, 11),
    (2, 5, 9, 0),
    (4, 7, 11, 2),
    (5, 9, 0, 4),
    (7, 11, 2, 5),
    (9, 0, 4, 7),
    (11, 2, 5, 9),
]

MAJOR_DIATONIC = [tuple(sorted(x)) for x in MAJOR_DIATONIC]

JAZZ_VOICINGS = [*MAJOR_DIATONIC]

REGENERATE_VOICINGS = True


from itertools import chain, combinations, combinations_with_replacement, product


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))


def generate_all_playable_fretboard_subsets():
    playable_voicings = []
    num_frets = 17
    num_situations = sum(math.comb(6, i) * (num_frets ** i) for i in range(6 + 1))
    # print(num_situations)
    situations_processed = 0
    for strings_used in powerset(GUITAR_STRINGS):
        for fret_comb in product(
            [i for i in range(num_frets)], repeat=len(strings_used)
        ):
            # if situations_processed % (num_situations // 100) == 0:
            # print(f"progress: {situations_processed/num_situations * 100} %")
            if len(fret_comb) >= 4:  # I only care about voicings with 4 or more
                string_to_fret_positions = {
                    strings_used[i]: fret_comb[i] for i in range(len(strings_used))
                }
                playable, barre_data = is_playable(string_to_fret_positions)
                if playable:
                    playable_voicings.append((string_to_fret_positions, barre_data))
        situations_processed += 1
    return playable_voicings


def is_playable(string_to_fret_positions):
    """
    largest x distance possible is 6
    largest y distance possible is 24
    These are the extents of the fretboard, set them to the opposite so they will change
    on the first iteration

    :param voicing:
    :return:
    """
    min_fret_pos = 24
    max_fret_pos = 0
    min_string_pos = 6
    max_string_pos = 0

    strings_used = list(string_to_fret_positions.values())

    # Assuming intervals.length >= 2
    # +1 because a single fret has area (not just a point) - a finger taking up space
    chord_width = strings_used[-1] - strings_used[0] + 1

    # You need at least one finger to play a note that isn't an open string
    num_non_open_strings_used = 0

    for string, fret in string_to_fret_positions.items():
        # Open strings don't count towards it
        if fret != 0:
            num_non_open_strings_used += 1

            min_fret_pos = min(min_fret_pos, fret)
            max_fret_pos = max(max_fret_pos, fret)

            min_string_pos = min(min_string_pos, string)
            max_string_pos = max(max_string_pos, string)

    # +1 because even a single fret has height (finger pressing on it)
    # Or else bar chords would have 0 area
    chord_height = max_fret_pos - min_fret_pos + 1
    # Don't need to add 1 here because the gaps between the strings are spaces
    chord_width = max_string_pos - min_string_pos

    chord_area = chord_width * chord_height

    valid_size = chord_area <= 24 and chord_height <= 5

    enough_fingers = True
    if len(strings_used) > 4:
        barre_data = construct_barre_data(string_to_fret_positions)

        can_use_barre = len(barre_data) != 0

        # assuming the player has 4 fingers available
        if num_non_open_strings_used > 4 and not can_use_barre:
            enough_fingers = False

        return valid_size and enough_fingers, barre_data

    return valid_size and enough_fingers, None


def construct_barre_data(string_to_fret_positions):
    """
    This function takes a voicing and constructs barres
    out of the fret positions, it constructs the start
    and end positions and if it is blocking other notes
    see the comment A for more information about the output

    It stores this in the voicing under the parameter fret_to_barre_data
    """

    # Of the form {fretNum: occurance}
    fret_to_occurance = {}

    for string, fret in string_to_fret_positions.items():
        # Open strings don't even need fingers
        if fret != 0:
            if fret not in fret_to_occurance:
                fret_to_occurance[fret] = 0
            fret_to_occurance[fret] += 1

    """
    for any fret number which is used more than once, iterate over the strings from thickest to thinnest
    and once you find the first occurence you set a flag and then start checking if the next fret numbers are
    are all great than or equal to it, if we find one that is lower it breaks the barre and so 
    long line situations where they're all part of the barre
    """

    # (A)
    # This variable maps a fret number to a list of the form
    # [ [barreArray], blockingBarre]
    # And the barreArray is an array of the form
    # [x,y] where x and y are start and end poisitions of the barre

    fret_to_barre_data = {}

    """
    
          X
    X X X   X X
    0 1 2 3 4 5 
    
        sub_barre_possible
    0: False - nothing gets added
    1: True  - [0, 1] added
    2: True  - [1, 2] added
    3: False - nothing added
    4: True  - [3, 4] added
    5: True  - [4, 5] added
    
    net result [0, 1], [1, 2], [3, 4], [4, 5]
    
    blocking_barre is False
    
              X 
            X  
    X X X X    
    0 1 2 3 4 5 
    
        sub_barre_possible
    0: False - nothing gets added
    1: True  - [0, 1] added
    2: True  - [1, 2] added
    3: True  - [2, 3] added - occurances_left is 0
    4: True  - [3, 4] added
    5: True  - [4, 5] added
    
    net result [0, 1], [1, 2], [3, 4], [4, 5]
    
    blocking_barre is True
    """

    for fret, occurance in fret_to_occurance.items():
        if occurance >= 2:
            occurances_left = occurance
            blocking_barre = False
            # So we don't add on first finding of fretNumber
            sub_barre_possible = False
            first_instance_found = False

            for i in range(6):
                if i in string_to_fret_positions:
                    curr_fret = string_to_fret_positions[i]
                    if curr_fret == fret:
                        first_instance_found = True
                        if sub_barre_possible:
                            if fret not in fret_to_barre_data:
                                fret_to_barre_data[fret] = [[]]
                            fret_to_barre_data[fret][0].append([barre_start, i])
                        occurances_left -= 1
                        barre_start = i  # this is usually going to represent the previous iteration's index
                        sub_barre_possible = True
                    else:
                        if occurances_left != 0:
                            if first_instance_found:
                                curr_fret_does_not_break_barre = curr_fret >= fret
                                if not curr_fret_does_not_break_barre:
                                    sub_barre_possible = False
                        else:
                            # We're done searching for barres, we can now check to see if
                            if (
                                fret in fret_to_barre_data
                            ):  # this checks to see if we were able to find a barre
                                if curr_fret < fret and curr_fret != 0:
                                    blocking_barre = True

            if fret in fret_to_barre_data:
                fret_to_barre_data[fret].append(blocking_barre)

    fret_to_barre_data_final = {}
    for fret, barre_data in fret_to_barre_data.items():
        barre_intervals = barre_data[0]
        blocking_barre = barre_data[1]
        fret_to_barre_data_final[fret] = [
            connect_list_of_interals(barre_intervals),
            blocking_barre,
        ]

    return fret_to_barre_data_final


def connect_list_of_interals(intervals):
    """
    Given a list of intervals of the form
    [[x1,y1], [x2,y2], ..., [xn, yn]]
    where it is known that for any i:
               y(i-1) <= x(i)
    then return a new list of intervals
    where if it's possible to merge any intervals
    you do it.

    You would merge when y(i-1) = x(i)
    """
    merged_intervals = []
    just_merged = False

    if len(intervals) == 1:
        return intervals

    for i in range(len(intervals) - 1):
        previous_start = intervals[i][0]
        previous_end = intervals[i][1]
        # i + 1 is valid because of upperbound
        next_start = intervals[i + 1][0]
        next_end = intervals[i + 1][1]

        if previous_end == next_start:
            if just_merged:
                last_idx = len(merged_intervals) - 1
                just_merged_interval_start = merged_intervals[last_idx][0]
                just_merged_interval_end = merged_intervals[last_idx][1]

                merged_intervals[last_idx] = [just_merged_interval_start, next_end]
            else:
                merged = [previous_start, next_end]
                merged_intervals.append(merged)

            just_merged = True
        else:
            merged_intervals.append(intervals[i])
            just_merged = False

    return merged_intervals


def write_variable_to_file(var, filename):
    file = open(filename, "wb")
    pickle.dump(var, file)
    file.close()


if __name__ == "__main__":

    import pickle

    if REGENERATE_VOICINGS:
        voicings = []
        fretboard_subsets = generate_all_playable_fretboard_subsets()
        for string_to_fret_position, barre_data in fretboard_subsets:
            voicings.append(GuitarVoicing(string_to_fret_position))

        note_collection_to_voicings = {}

        for voicing in voicings:
            fundamental_note_collection = tuple(
                sorted(tuple(set(note.value % 12 for note in voicing.notes)))
            )
            if fundamental_note_collection not in note_collection_to_voicings:
                note_collection_to_voicings[fundamental_note_collection] = []
            note_collection_to_voicings[fundamental_note_collection].append(voicing)

        filename_to_data = {
            "note_collection_to_voicings.txt": note_collection_to_voicings,
        }

        for filename, data in filename_to_data.items():
            write_variable_to_file(data, filename)
    else:
        file = open("note_collection_to_voicings.txt", "rb")
        note_collection_to_voicings = pickle.load(file)

    jazz_note_collection_to_voicings = {}
    for note_collection, voicings in note_collection_to_voicings.items():
        if note_collection in JAZZ_VOICINGS:
            jazz_note_collection_to_voicings[note_collection] = voicings

    write_variable_to_file(
        jazz_note_collection_to_voicings, "jazz_note_collection_to_voicings.txt"
    )

    # generate_all_playable_fretboard_subsets()
    # print(construct_barre_data({0: 5, 1: 5, 2: 5, 3: 5, 4: 4, 5: 3}))
